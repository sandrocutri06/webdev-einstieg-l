let geld = 250;

function addMoney() {
  geld += 50;
  displayMoney();
}

function subMoney() {
  if (geld >= 50) {
    geld -= 50;
    displayMoney();
  }
}

function resetMoney() {
  geld = 0;
  displayMoney();
}

function displayMoney() {
  document.getElementById("balance").innerHTML =
    "Dein Kontostand beträgt: " + geld + ".- CHF";
}
